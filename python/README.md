# PYTHON

### [Basic CRUD - basic crud for a simple blog](https://gitlab.com/ninjahoahong/django-basic-crud)

A simple CRUD example using Django framework.

### [YAAS - a simple auction house](https://gitlab.com/ninjahoahong/django-yaas)

An auction house written using Django framework.
