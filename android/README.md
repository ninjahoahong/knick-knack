# Android Experiments

## [My Android Samples](https://gitlab.com/ninjahoahong/my-android-samples)

This repo contains different Android experiments. The subjects can be views, architectures, components, and functions

## [My Views](https://gitlab.com/ninjahoahong/myviews)

This is a collection of custom views to be copy-pasted depends on projects' needs.

## [Sat Wagen](https://gitlab.com/ninjahoahong/sat-wagen)

An attempt to build game engine on Android with the journey recorded.

## [Free Ebooks Reader](https://gitlab.com/ninjahoahong/free-ebooks-reader-android)

A tool to search for and read free ebooks using google api.

## [Suto GameSearch](https://gitlab.com/ninjahoahong/sutoen-gamesearch)

An android app to search your favorite games in g2a.com.
