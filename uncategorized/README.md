# UNCATEGORIZED

## [chaipik0](https://gitlab.com/ninjahoahong/chaipik0)

Chaipik0 is a simple chat server application written in Erlang.

## [Manga Scraper Functions](https://gitlab.com/ninjahoahong/manga-scraper-functions)

A set of utility functions to scrap manga online

## [Kitura Chat Server](https://gitlab.com/ninjahoahong/chat-samples/-/tree/master/chat-server)

A Chat Server App built with Kitura (a swift framework to build server application).

## [Angular github app](https://gitlab.com/ninjahoahong/angular-github-app)

A github web client built with Angular.
