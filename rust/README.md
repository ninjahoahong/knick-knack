# Rust

## [Hello Web Assembly](https://gitlab.com/ninjahoahong/hello-webAssembly)

Helloworld example for web assembly using Rust.

## [Open Whisk](https://gitlab.com/ninjahoahong/open-whisk)

Trying Open Whisk using Rust api.

## [Simple youtube downloader](https://gitlab.com/ninjahoahong/simple_rust_youtube_downloader)

A helloworld to rust cli app.

