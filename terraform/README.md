# Terraform

## [Terraform examples for Azure](https://gitlab.com/ninjahoahong/azure-terraform-samples)

Experiments building infrastructure as code on Azure using Terraform.

## [Vault example](https://gitlab.com/ninjahoahong/vault-example)

Set up Hashicorp Vault Development in Azure using Terraform.

## [Learn Terraform](https://gitlab.com/ninjahoahong/learn-terraform)

A getting started point for terraform.
