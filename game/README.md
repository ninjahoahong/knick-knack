# Game development experiments

## [Unstoppable](https://gitlab.com/ninjahoahong/unstoppable)

A quiz

## [Get The Key](https://gitlab.com/ninjahoahong/get-the-key)

This is one of my designed board game. It is a simple dice based game to play in a small-group party.

## [O An Quan](https://gitlab.com/ninjahoahong/OAnQuan)

O An Quan is a traditional Vietnamese game which use stones as a unit to play. This is my simple implementation using vannilla java script.

## [Real Time White Board](https://gitlab.com/ninjahoahong/rt-whiteboard)

This is a very simple realtime colaboration white board for playing together. The example uses expressjs, sketchjs, and socketio.

## [Study Game Dev](https://gitlab.com/ninjahoahong/study-game-dev)

This is a collection of resources such as books, tutorials, and free arts for studying game development.


## [Space Invadder](https://gitlab.com/ninjahoahong/space-invader)

Space invader game implementation in clojure.

## [Space Game](https://gitlab.com/ninjahoahong/space-game)

A game with a space theme. This game is built using JMonkey Game Engine.
