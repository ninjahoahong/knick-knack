# KNICK KNACK
This repository contains all my computer software experiments' descriptions and link to the experiments' repositories. I am using them as my references to learn new things. Hopefully some of them are useful to others as well. There are a lot of about-to-do and incomplete stuffs around. Therefore, I also some time trying to explain stuffs in some of [my blog](https://ninjahoahong.com) posts.

# LICENSE 
* The code in this repository is distributed under an MIT license.
* Concept or materials are distributed under a [Creative Commons Attribution 4.0 International License](https://creativecommons.org/licenses/by/4.0/) - [![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)
* Individual projects have their own licenses spcified in the repositories.
