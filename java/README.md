# Jav experiments

## [Spring Oauth2 Authorization Server](https://gitlab.com/ninjahoahong/spring-oauth2-authorization-server)

An Oauth2 Authorization Server Sample with Spring Boot, Spring Security, Gradle, Postgresql, and Flyway.

* What in the examples:
    * Provide JWT
    * Auto database migration with Flyway
    * Build with gradle
    * Running locally with Docker + docker-compose
    * Store data in Postgresql database
    * Init database with startup sql-script

## [Spring Boot Staring Project](https://gitlab.com/ninjahoahong/spring-boot-example)

A starting project example for Spring Boot 2 with Postgresql, Swagger2, and JWT

* What in the examples:
    * Simple sign up and login.
    * Simple Rest API demostration.
    * Auto database migration with Flyway.
    * Auto documentation generation with Swagger2
    * Build with gradle.
    * Running locally with Docker + docker-compose.
    * Store data in Postgresql database.
    * Init database with startup sql-script.
    * Some unit test for the Rest API.

## [Todo Recipe](https://gitlab.com/ninjahoahong/todo-recipe)
These are apis to share your todo list as a model/recipe for everyone to follow and improve. This example built using Google Cloud Endpoint Java SDK, Google AppEngine, and Google DataStore with Objectify.

* What in the examples:
    * Simple login with Facebook
    * Todo Recipe and Categories API
    * Documentation with [API Blue Print](https://apiblueprint.org/)


## [Java for data scientists](https://gitlab.com/ninjahoahong/java-for-data-scientists)

A collection of useful java example for data scientists for example using java for scripting a commandline tool, manipulating dataframe, running a simple server, and training models.

## [Undertow Samples](https://gitlab.com/ninjahoahong/undertow-samples)

Samples of building web application with Jboss' Undertow

## [Simple Ecommerce API](https://gitlab.com/ninjahoahong/simple-ecommerce-api)

Build ecommerce application with Google App Engine/Google Cloud Endpoint.

## [Jav1kehe](https://gitlab.com/ninjahoahong/jav1kehe)

Jav1kehe is a 3d map editor written in Java.

